$ ->
	if $('.authorization').length > 0
		$('.authorization-header').on 'click', ->
			$(this).addClass('active')
			$(this).siblings('.authorization-header').removeClass('active')
			elem_to_open = $(".#{$(this).attr('for')}")
			elem_to_open.siblings('.authorization-body').fadeOut 0, ->
				elem_to_open.fadeIn()
				$('input:first').focus()

	if $('#main-recent-list').length > 0
		GarantHelper.hover_on $('#main-recent-list table tbody'), 'tr'

	if $('#account-notification').length > 0
		$('#account-notification .close').on 'click', ->
			$('#account-notification').slideUp('slow')

	if $('#letter-main').length > 0
		$('#letter-main').on 'click', '.sign-show-on-doc', ->
			$(this).next().toggle()

	if $('#main-buttons-div').length > 0
		$('.main-button-background').on 'mouseenter', ->
			$(this).children().addClass('hover');
		$('.main-button-background').on 'mouseleave', ->
			$(this).children().removeClass('hover');

	if $('#main-recent-div').length > 0
		GarantHelper.hover $('#main-button-new, #main-button-inbox, #main-button-outbox, #main-button-address, #main-button-archive')

	if $('#side-address').length > 0
		$('#side-address-search-content').gdefv()
		GarantHelper.hover_on $('#side-address-list'), '.side-address-line'
		GarantHelper.hover $('#show-hide-side')
		$('#side-address-list')
		.on 'mouseenter', '.org-line', ->
			if $(this).find('.side-address-group:hidden').length > 0
				$(this).find('.side-address-group').show 'blind', { direction:'vertical' }, 50
		.on 'mouseleave', '.org-line', ->
			if $(this).find('.side-address-line.active').length == 0
				$(this).find('.side-address-group').hide 'blind', { direction:'vertical' }, 50
		GarantHelper.hover_on $('#side-address-list'), '.side-address-sub'

		$('#side-address-search-right').click ->
			GarantHelper.search $('#side-address-search-content'), $('#side-address-search-right'), 2, loadSideByParams
		$('#side-address-search-content').keypress (event) ->
			if event.which == 13
				GarantHelper.search $('#side-address-search-content'), $('#side-address-search-right'), 1, loadSideByParams

@activateLine = ->
	GarantHelper.hover $('.main-recent-line:last')

@initOneLineInfo = (divID, client) ->
	GarantHelper.hover $('#' + divID + '-link')

	$('#' + divID + '-link').click (event) ->
		$.ajax
			url: '/clients/info'
			data:
				id: client
				x: event.pageX
				y: event.pageY

@initSideAddress = ->
	GarantHelper.animate_on $('#side-address-list'), '#side-address-more'

	@setActive = (elem) ->
		prev = $('#side-address-list .active')
		prev.removeClass 'active'
		if prev.parent().hasClass 'side-address-group'
			prev.parent().css 'display', 'none'
			prev.parent().parent().mouseleave ->
				$(this).find('.side-address-group').hide 'blind', {direction: 'vertical'}, 200
		elem = $(elem)
		elem.addClass 'active'
		if elem.parent().hasClass 'side-address-group'
			elem.parent().css 'display', 'block'
			elem.parent().parent().unbind 'mouseleave'
		$('#side-current-client')
			.attr('data-type', elem.attr('data-type'))
			.attr('data-id',   elem.attr('data-id'))
			.attr('data-guid', elem.attr('data-guid'));

	@loadSideByParams = (params = null) ->
		params = GarantHelper.getCurrentParams() unless params
		$.progressBar text: 'Идёт построение списка контрагентов...', endless: true
		$.ajax
			url: '/side/load_list'
			data: params
			success: ->
				$.progressBar 'destroy'

	@loadLetterByParams = (params = null, moreFlag = false) ->
		params = GarantHelper.getCurrentParams() unless params
		if moreFlag
			$('#letter-more').remove()
			params.last_id = $('.letter-table-line:last').attr 'data-msg'
		$.progressBar text: 'Идёт построение списка документов...', endless: true
		$.ajax
			url: '/letters/load_list'
			data: params
			success: ->
				$.progressBar 'destroy'
				document.getElementById('letter-table').scrollTop += 300 if moreFlag

	@loadMoreSideByParams = (params = null) ->
		$('#side-address-more').remove()
		params = GarantHelper.getCurrentParams() unless params
		params.count = $('.side-address-line[data-type!="group"]').length
		$.progressBar text: 'Идёт построение списка контрагентов...', endless: true
		$.ajax
			url: '/side/more_list'
			data: params
			success: ->
				$.progressBar 'destroy'
