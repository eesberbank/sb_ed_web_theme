$ ->
  $(document).click (e) ->
    forget_password = $(".forget-password")
    if forget_password.length > 0
      if $('.forget-password-background').is(e.target)
        $('.forget-password').fadeOut()

    auth_form = $('.authorization')
    if auth_form.length > 0 && $('#login_ng').length > 0
      if $('.authorization-background').is(e.target)
        $('.authorization-form').animate
          opacity: 0,
          marginTop: '-600px',
          'fast',
          'linear',
          ->
            $('.authorization').fadeOut 'fast'

    certificate_list = $(".certificate-chose")
    if certificate_list.length > 0
      if $('.certificate-container').is(":visible") &&
      !certificate_list.is(e.target) &&
      certificate_list.has(e.target).length == 0
        $('.certificate-container').toggle 'slide', direction: 'up'

  $('.registration-form-close').click ->
    ClientRegistration.close()

  $('#dss_login').keypress (e) ->
    if e.keyCode == 13 or e.which == 13
      Login.enterByLogin()

  $('#dss_password').keypress (e) ->
    if e.keyCode == 13 or e.which == 13
      Login.enterByLogin()

  $('#dss_sms').keypress (e) ->
    if e.keyCode == 13 or e.which == 13
      Login.approveCode()

  if $('#dss_phone').length > 0
    $('#dss_phone').keydown (e) ->
      e.preventDefault()
      real_phone = $(this).attr('real-phone')

      if e.keyCode >= 48 && e.keyCode <= 57 && real_phone.length < 11
        digit = parseInt(e.keyCode) - 48
        real_phone = "#{real_phone}#{digit}"
      if e.keyCode == 8
        real_phone = real_phone.substring(0, real_phone.length - 1)

      $(this).attr('real-phone', real_phone)

      shown_phone = ""
      if real_phone.length > 0
        shown_phone = "+#{real_phone.substring(0, 1)}"
      if real_phone.length > 1
        shown_phone = "#{shown_phone} (#{real_phone.substring(1, 4)}"
      if real_phone.length > 4
        shown_phone = "#{shown_phone}) #{real_phone.substring(4, 7)}"
      if real_phone.length > 7
        shown_phone = "#{shown_phone}-#{real_phone.substring(7, 9)}"
      if real_phone.length > 9
        shown_phone = "#{shown_phone}-#{real_phone.substring(9, 11)}"

      $(this).attr('shown_phone', shown_phone)
      $(this).val(shown_phone)

  if $('#login-sb-continue').length > 0
    GarantHelper.animate $('#login-sb-continue')

  if $('#login-ng, #login-sb').length > 0
    $(document).ready ->
      CryptoAdapter.init(Login.cryptoInitCallback, true)

  if $('#login-video-img').length > 0
    $('#login-ng-video-div').on 'mouseenter', '#login-video-img', ->
      $(this).attr 'src', '/assets/login_ng/video_2.png'
    .on 'mouseleave', '#login-video-img', ->
      $(this).attr 'src', '/assets/login_ng/video_1.png'
    .on 'click', '#login-video-img', ->
      window.open 'http://vimeo.com/50047368', '_blank'
      window.focus()
  if $('#login-calc').length > 0
    GarantHelper.animate $('#login-calc-back')
    $('#login-calc-form .login-calc-data-input[data-defvalue]').gdefv()
  $("#login-variants").tabs()
  $('input, textarea').placeholder()

class @Login
  @ban_timeout
  @tries = 1
  @certificates = []
  @isUserNotificated

  @toggle_enter = ->
    $('.authorization').fadeIn 'fast', ->
      $('.authorization-form').animate
        opacity: 100,
        marginTop: '',
        'fast',
        'linear'        

  @authorize = ->
    $('.registration').fadeOut()
    $('.authorization').fadeIn()

  @register = ->
    $('.registration').fadeIn()
    $('.authorization').fadeOut()

  @ban_countdown = ->
    if $('#ban-countdown').length != 0
      clearTimeout Login.ban_timeout
      seconds = parseInt($('#ban-countdown').html())

      callback = ->
        seconds = seconds - 1
        if seconds == 0
          location.reload()
        if seconds < 0
          $('#ban-countdown').parent().parent().html(
            'Подождите несколько секунд до следующей попытки входа'
          )
        else
          $('#ban-countdown').html("#{seconds}")
          Login.ban_countdown()

      Login.ban_timeout = setTimeout callback, 1000

  @forget = ->
    Login.switch_authy('.authentication', '.restore_password')
    $('.forgot-pants').html('Вернуться')
    $('.forgot-pants').attr('onClick', 'Login.remember()')
    $('.auth_button').html('Восстановить пароль')
    $('.auth_button').attr('onClick', 'Login.restore_password()')

  @remember = ->
    Login.switch_authy('.restore_password', '.authentication')
    $('.forgot-pants').html('Забыли пароль?')
    $('.forgot-pants').attr('onClick', 'Login.forget()')
    $('.auth_button').html('Войти')
    $('.auth_button').attr('onClick', 'Login.enterByLogin()')

  @send_sms_again = ->
    Service.alert("Не собираемся пока спамить смсками")

  @switch_authy = (element_to_hide, element_to_show) ->
    $(element_to_hide).slideUp "fast", ->
      $(element_to_show).slideDown "fast"

  @newVersionAlert = ->
    if Promise and Promise.resolve
      Service.confirm(
        'Обновление КриптоЭкспресс',
        'Доступна новая версия утилиты КриптоЭкспресс',
        type: 'info'
        confirmButtonText: '<span class="material-control"><i class="tiny material-icons download-button inline"></i> Скачать</span>'
        cancelButtonText: 'Отмена'
      ).then (e) ->
        if e.value
          window.open CE_INSTALL_URI, '_blank'
          window.focus()
    else
      $('#new-ce-version').remove()
      $('body').append $('<div/>',
        id: 'new-ce-version'
        style: 'text-align: center;'
        title: 'Обновление КриптоЭкспресс'
        text: 'Доступна новая версия утилиты КриптоЭкспресс'
      )

      dialogOpts = {
        modal: true
        resizable: false
        width: 300
        buttons:
          "Скачать": ->
            window.open CE_INSTALL_URI, "_blank"
            window.focus()
          "Отмена": ->
            $('#new-ce-version').dialog "close"
      }
      $('#new-ce-version').dialog dialogOpts

  @cryptoExpressNotAvailable = ->
    $('.error-checkout').html(
      $('<p/>', class: 'alert', text: 'Утилита КриптоЭкспресс недоступна')
    ).append(
      $('<p/>', text: 'Запустите утилиту или установите')
    ).append(
      $('<p/>').append(
        $('<a/>', href: CE_INSTALL_URI, target: '_blank', text: 'новую версию')
      )
    )

    console.warn 'Необходимая версия утилиты КриптоЭкспресс:', "#{CE_VERSION}", 'или выше'
    !Login.isUserNotificated and Login.isUserNotificated = true

  @loadCert = (elem, thumbprint) ->
    callback = (result) ->
      if result.status == 200
        $(elem).val(result.payload.certificate)
      else
        Service.alert(
          "Не удалось загрузить информацию о сертификате\n#{result.payload}"
        )

    CryptoAdapter.do 'loadCert', callback, {thumbprint: thumbprint}

  @loadCertsCallback = (certs) ->
    str = '
<div class="certificate-panel">
  <div class="certificate-chose">
  	<div class="certificate-chose-input text">
  		<div class="certificate-chosen">
        <div class="placeholder">
          Выбор сертификата
        </div>
      </div>
  		<span class="caret"></span>
  	</div>
  	<div class="certificates-wrapper">
  		<div class="certificate-container"></div>
  	</div>
    <div class="authorization-error"></div>
  </div>
</div>
<div class="action-button blue authorization-button" onclick="Login.enterTheSite()">
	Войти
</div>'

    $('.authorization-by-cert').html(str)

    $('.certificate-chose-input').on 'click', ->
      $('.certificate-container').toggle 'slide', direction: 'up'

    $('.preloader').remove()
    $('.error-checkout').remove()

    if parseFloat(Cookies.get('ce_ver')) >= CE_VERSION
      ClientRegistration.set_step_1
        result: 'success'
    else
      ClientRegistration.set_step_1
        result: 'fail'
        reason: 'refresh_ce'

    show_certificates = (checked_certificates) ->
      certificate_filter = {}

      $.each certs, (_, certificate) ->
        if client_id = checked_certificates[certificate.thumbprint]
          cert = certificate_filter[client_id] ?= certificate
          if cert != certificate and (
            (certificate.working and not cert.working) or
              (certificate.validToInt > cert.validToInt)
          )
            certificate_filter[client_id] = certificate
        else if certificate.working
          ClientRegistration.certificates.push certificate

      certsArr = $.map certificate_filter, (cert) -> cert

      Login.certificates = certsArr.sort (a, b) ->
        if a.working and not b.working
          -1
        else if not a.working and b.working
          1
        else
          0

      for cert in Login.certificates
        $('.certificate-container').append(
          $('<div/>',
            class: 'certificate-option tooltipping material-control'
            'data-wor': cert.working
            'data-cert-serial': cert.serial
            'data-cert-subj': cert.subject
            'data-cert-iss': cert.issuer
            'data-cert-valid': cert.validTo
            'data-tp': cert.thumbprint
          ).append(
            $('<div/>',
              class: 'material-control'
              text: cert.subject
            ).prepend(
              $('<i/>',
                class: "material-icons inline cert-status #{
                  if cert.working then 'active check-icon' else 'inactive error-icon'
                }")
            )
          )
        )

      $('.certificate-option')
        # .click ->
        #   if $(this).hasClass('disclosed')
        #     $(this).removeClass('disclosed')
        #     $(this).children('.certificate-detail').clearQueue()
        #     $(this).children('.certificate-detail').slideUp ->
        #       $(this).remove()
        #
        #     tmp = $(this).clone()
        #     tmp.children('.certificate-detail').remove()
        #     $('#login-ng-thumbprint').val tmp.attr 'data-tp'
        #     $('#login-serial').val tmp.attr 'data-cert-serial'
        #     $('.certificate-chosen').html tmp
        #     $(".certificate-container").toggle()
        #   else
        #     $('.certificate-option.disclosed').each ->
        #       $(this).removeClass('disclosed')
        #       $(this).children('.certificate-detail').clearQueue()
        #       $(this).children('.certificate-detail').slideUp ->
        #         $(this).remove()
        #
        #     $(this).addClass('disclosed')
        #
        #     if $(this).attr('data-wor') == 'true'
        #       str = '<p class="image active">Сертификат действителен</p>'
        #     else
        #       str = '<p class="image inactive">Сертификат недействителен</p>'
        #     $(this).append(
        #       "<div class='certificate-detail'>
        #         <p>Выдан кем: #{$(this).attr('data-cert-iss')}</p>
        #         <p>Выдан кому: #{$(this).attr('data-cert-subj')}</p>
        #         <p>Годен до: #{$(this).attr('data-cert-valid')}</p>
        #         #{str}
        #       <div>"
        #     )
        #     $(this).children('.certificate-detail').slideDown()

        .mouseenter ->
          $(this).clearQueue()
          if $(this).attr('data-wor') == 'true'
            str = '<p class="image active">Сертификат действителен</p>'
          else
            str = '<p class="image inactive">Сертификат недействителен</p>'
          $(this).append(
            "<div class='certificate-detail'>
              <p>Выдан кем: #{$(this).attr('data-cert-iss')}</p>
              <p>Выдан кому: #{$(this).attr('data-cert-subj')}</p>
              <p>Годен до: #{$(this).attr('data-cert-valid')}</p>
              #{str}
            <div>"
          )
          $(this).children('.certificate-detail').delay( 350 ).slideDown()
        .mouseleave ->
          $(this).clearQueue()
          $(this).children('.certificate-detail').clearQueue()
          $(this).children('.certificate-detail').slideUp ->
            $(this).remove()
        .click ->
          tmp = $(this).clone()
          tmp.children('.certificate-detail').remove()
          $('#login-ng-thumbprint').val tmp.attr 'data-tp'
          $('#login-serial').val tmp.attr 'data-cert-serial'
          $('.certificate-chosen').html tmp
          $(".certificate-container").toggle 'slide', direction: 'up'

    thumbs = $.map certs, (elem, index) ->
      elem.thumbprint

    $.ajax
      url: "/certificate/check"
      type: 'POST'
      beforeSend: (xhr) ->
        xhr.setRequestHeader(
          'X-CSRF-Token',
          $('meta[name="csrf-token"]').attr('content')
        )
      data:
        thumbprints: thumbs
      complete: ->
        $.progressBar 'destroy'
      success: (result) ->
        show_certificates(result)
      error: (result) ->
        console.log 'Error connecting to sedo'

  @cryptoInitCallback = (cryptoState) ->
    if cryptoState == 'ok'
      unless CryptoAdapter.isActualCE
        Login.newVersionAlert()
      $('.error-checkout').html('Загрузка сертификатов...')
      CryptoAdapter.loadCerts Login.loadCertsCallback
    else
      callback = ->
        CryptoAdapter.init Login.cryptoInitCallback, true
      setTimeout callback, 3000
      unless Login.isUserNotificated
        Login.cryptoExpressNotAvailable()

  @verifyClient = ->
    thumbprint = $('#login-thumbprint').val()
    if thumbprint
      $('#login-status').html 'Проверка сертификата...'
      $.progressBar text: 'Идентификация пользователя...', endless: true
      try
        enterSiteCallback = (result) ->
          $.progressBar 'destroy'
          if result.status == 200
            $('#login-signed').val result.payload.signature
            $('#login-form').submit()
          else
            $('#login-alert').html result.payload
            $('#login-status').html(result.extra || '').append(
              $('<p/>', text: 'Вы можете скачать последнюю версию утилиты КриптоЭкспресс ').append(
                $('<a/>', href: CE_INSTALL_URI, target: '_blank', text: 'здесь')
              )
            )

        payRequest = {thumbprint:thumbprint, data:$('#login-text').val()}

        CryptoAdapter.do 'signFile', enterSiteCallback, payRequest
      catch e
        $.progressBar 'destroy'
        $('#login-alert').html e.description
        $('#login-status').html ''
    else
      $('#login-alert').html 'Не выбран сертификат'
      $('#login-status').html ''

  @forgetPassword = ->
    $(".forget-password").fadeIn()

  @enterTheSite = ->
    thumbprint = $('#login-ng-thumbprint').val()
    if thumbprint.length > 0
      $('.authorization-error').html(
        '<div class="neutral">Проверка сертификата...</div>'
      )

      try
        enterSiteCallback = (result) ->
          if result.status == 200
            $('#login-signed').val result.payload.signature
            $('#login-form').submit()
          else
            $('.authorization-error').html result.payload

        payRequest = {
          thumbprint: thumbprint,
          data: $('#login-text').val()
        }

        CryptoAdapter.do 'signFile', enterSiteCallback, payRequest
      catch e
        $('.authorization-error').html e.description
    else
      $('.authorization-error').html 'Не выбран сертификат'

  @showDssError = (message) ->
    Service.alert(message)

  @restore_password = ->
    $.ajax
      url: "/dss/reset"
      type: 'POST'
      beforeSend: (xhr) ->
        xhr.setRequestHeader(
          'X-CSRF-Token',
          $('meta[name="csrf-token"]').attr('content')
        )
      data:
        login: base64.encode($('#restore_login').val())
        phone: base64.encode($('#dss_phone').attr('real-phone'))
      complete: ->
        $.progressBar 'destroy'
      success: (result) ->
        Login.remember()
        Service.alert("Новый пароль будет выслан на указанный телефон")

      error: (result) ->
        errorDesc = JSON.parse(result.responseText).error
        Login.showDssError(
          "Ошибка восстановления пароля: #{errorDesc}"
        )


  @approveCode = ->
    if $('#dss_sms').val().length == 0
      Service.alert('Ошибка входа: Необходимо ввести код из сообщения')
      return

    $.progressBar text: 'Авторизация на сервере', endless: true
    $.ajax
      url: "/dss/confirm"
      type: 'POST'
      beforeSend: (xhr) ->
        xhr.setRequestHeader(
          'X-CSRF-Token',
          $('meta[name="csrf-token"]').attr('content')
        )
      data:
        sms_code: base64.encode($('#dss_sms').val())
      complete: ->
        $.progressBar 'destroy'
      success: (result) ->
        $('#dss_sign_text').val(result.sign)
        $('#dss_serial').val(result.sn)
        $('#dss_thumbprint').val(result.thumbprint)
        $('#login-auth').submit()
      error: (result) ->
        if JSON.parse(result.responseText).reload
          location.reload()
        errorDesc = JSON.parse(result.responseText).error
        Login.showDssError("Ошибка входа: #{errorDesc}")

  @enterByLogin = ->
    if $('#dss_login').val().length == 0 or $('#dss_password').val().length == 0
      Service.alert(
        'Ошибка входа: Необходимо заполнить поле "Логин" и "Пароль"'
      )
      return

    $.progressBar text: 'Ожидание ответа от сервера', endless: true
    $.ajax
      url: "/dss/auth"
      type: 'POST'
      beforeSend: (xhr) ->
        xhr.setRequestHeader(
          'X-CSRF-Token',
          $('meta[name="csrf-token"]').attr('content')
        )
      data:
        login: base64.encode($('#dss_login').val())
        password: base64.encode($('#dss_password').val())
      complete: ->
        $.progressBar 'destroy'
      success: (result) ->
        $('.login-input').css('display', "none")
        $('.password-input').css('display', "none")
        $('.sms-code-input').css('display', "block")

        $('.forgot-pants').html('Отправить повторно')
        $('.forgot-pants').attr('onClick', 'Login.send_sms_again()')
        $('.authorization-button').html('Проверить код')
        $('.authorization-button').attr('onClick', 'Login.approveCode()')
        $('#dss_sms').focus()
      error: (result) ->
        errorDesc = JSON.parse(result.responseText).error
        Login.showDssError("Ошибка входа: #{errorDesc}")
        Login.ban_countdown()

  @toggleCalc = (flag) ->
    if flag
      $('#login-ng-fixed-bg').hide 'slide', {direction: 'left'}, 1000
      $('#login-ng-calc-bg').show 'slide', {direction: 'right'}, 1000
    else
      $('#login-ng-calc-bg').hide 'slide', {direction: 'right'}, 1000
      $('#login-ng-fixed-bg').show 'slide', {direction: 'left'}, 1000

  @checkCalcValue = (field) ->
    val = field.val()
    num = new Number val
    if isNaN(num) || val.length == 0 || num < 0
      field.addClass 'error'
      return null
    else
      field.removeClass 'error'
      return num

  @ifNaN = (elem, value, percent = false) ->
    if isNaN value
      elem.html '&mdash;'
    else
      if percent
        elem.text value.toFixed(0) + ' %'
      else
        elem.text value.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ") +
        ' руб.'

  @calc = () ->
    client_num = Login.checkCalcValue $('#login-calc-form input:eq(0)')
    docs_client_year = Login.checkCalcValue $('#login-calc-form input:eq(1)')
    salary = Login.checkCalcValue $('#login-calc-form input:eq(2)')
    lists_doc_do = Login.checkCalcValue $('#login-calc-form input:eq(3)')
    docsend_price_do = Login.checkCalcValue $('#login-calc-form input:eq(4)')
    list_pack_price = Login.checkCalcValue $('#login-calc-form input:eq(5)')
    cartridge_price = Login.checkCalcValue $('#login-calc-form input:eq(6)')
    cartridge_value = Login.checkCalcValue $('#login-calc-form input:eq(7)')

    return unless client_num &&
      docs_client_year &&
      salary &&
      lists_doc_do &&
      docsend_price_do &&
      list_pack_price &&
      cartridge_price &&
      cartridge_value

    admcoef = 1.7
    courier_docs = 0.1
    speedpost_docs = 0.2
    doc_create1_do_min = 15
    doc_courier_prep1_min = 5
    doc_speedpost_prep1_min = 10
    doc_post_prep1_min = 10
    do_border = 500
    doc_create2_do_min = 2
    doc_courier_prep2_min = 5
    doc_speedpost_prep2_min = 10
    doc_post_prep2_min = 10
    doc_create_edo_min = 5
    doc_edo_prep_courier_min = 2
    doc_edo_prep_speedpost_min = 2
    doc_edo_prep_post_min = 2
    lists_doc_edo = 0.5
    doc_price_edo = 8
    edo_border = 6000

    docs_year = Validator.roundFloatNumber docs_client_year * client_num, 4
    lists_year_do = Validator.roundFloatNumber lists_doc_do * docs_year, 4
    cartridge_year_do = Validator.roundFloatNumber lists_year_do / cartridge_value, 4
    lists_year_edo = Validator.roundFloatNumber docs_year * lists_doc_edo, 4
    cartridge_year_edo = Validator.roundFloatNumber lists_year_edo / cartridge_value, 4
    MH = Validator.roundFloatNumber salary * admcoef / (20 * 8), 4
    post_docs = Validator.roundFloatNumber 1 - courier_docs - speedpost_docs, 4
    doc_prep1_do_min = Validator.roundFloatNumber courier_docs * doc_courier_prep1_min + speedpost_docs * doc_speedpost_prep1_min + post_docs * doc_post_prep1_min, 4
    doc_create1_do_hour = Validator.roundFloatNumber doc_create1_do_min / 60, 4
    doc_courier_prep1_hour = Validator.roundFloatNumber doc_courier_prep1_min / 60, 4
    doc_speedpost_prep1_hour = Validator.roundFloatNumber doc_speedpost_prep1_min / 60, 4
    doc_post_prep1_hour = Validator.roundFloatNumber doc_post_prep1_min / 60, 4
    doc_prep1_do_hour = Validator.roundFloatNumber doc_prep1_do_min / 60, 4
    doc_prep2_do_min = Validator.roundFloatNumber courier_docs * doc_courier_prep2_min + speedpost_docs * doc_speedpost_prep2_min + post_docs * doc_post_prep2_min, 4
    doc_create2_do_hour = Validator.roundFloatNumber doc_create2_do_min / 60, 4
    doc_courier_prep2_hour = Validator.roundFloatNumber doc_courier_prep2_min / 60, 4
    doc_speedpost_prep2_hour = Validator.roundFloatNumber doc_speedpost_prep2_min / 60, 4
    doc_post_prep2_hour = Validator.roundFloatNumber doc_post_prep2_min / 60, 4
    doc_prep2_do_hour = Validator.roundFloatNumber doc_prep2_do_min / 60, 4
    doc_prep_edo_min = Validator.roundFloatNumber courier_docs * doc_edo_prep_courier_min + speedpost_docs * doc_edo_prep_speedpost_min + post_docs * doc_edo_prep_post_min, 4
    doc_create_edo_hour = Validator.roundFloatNumber doc_create_edo_min / 60, 4
    doc_edo_prep_courier_hour = Validator.roundFloatNumber doc_edo_prep_courier_min / 60, 4
    doc_edo_prep_speedpost_hour = Validator.roundFloatNumber doc_edo_prep_speedpost_min / 60, 4
    doc_edo_prep_post_hour = Validator.roundFloatNumber doc_edo_prep_post_min / 60, 4
    doc_prep_edo_hour = Validator.roundFloatNumber doc_prep_edo_min / 60, 4
    if docs_year / 12 > do_border
      MH_do_year = Validator.roundFloatNumber docs_year * (doc_create2_do_hour + doc_prep2_do_hour), 4
    else
      MH_do_year = Validator.roundFloatNumber docs_year * (doc_create1_do_hour + doc_prep1_do_hour), 4
    MH_edo_year = Validator.roundFloatNumber docs_year * (doc_create_edo_hour + doc_prep_edo_hour), 4
    list_price = Validator.roundFloatNumber list_pack_price / 500, 4

    postcosts_do_year = Validator.roundFloatNumber docs_year * docsend_price_do, 4
    MHcosts_do_year = Validator.roundFloatNumber MH_do_year * MH, 4
    papercosts_do_year = Validator.roundFloatNumber list_price * lists_year_do + cartridge_price * cartridge_year_do, 4
    overallcosts_do_year = Validator.roundFloatNumber postcosts_do_year + MHcosts_do_year + papercosts_do_year, 4
    if docs_year > edo_border
      postcosts_edo_year = Validator.roundFloatNumber (docs_year - edo_border) * doc_price_edo * (courier_docs + speedpost_docs + post_docs), 4
    else
      postcosts_edo_year = 0
    MHcosts_edo_year = Validator.roundFloatNumber MH_edo_year * MH, 4
    papercosts_edo_noeds_year = Validator.roundFloatNumber list_price * lists_year_edo + cartridge_price * cartridge_year_edo, 4
    overallcosts_edo_noeds_year = Validator.roundFloatNumber postcosts_edo_year + MHcosts_edo_year + papercosts_edo_noeds_year, 4
    postcosts_economy_percent = Validator.roundFloatNumber (1 - postcosts_edo_year / postcosts_do_year) * 100, 4
    MHcosts_economy_percent = Validator.roundFloatNumber (1 - MHcosts_edo_year / MHcosts_do_year) * 100, 4
    papercosts_economy_noeds_percent = Validator.roundFloatNumber (1 - papercosts_edo_noeds_year / papercosts_do_year) * 100, 4
    overallcosts_economy_noeds_percent = Validator.roundFloatNumber (1 - overallcosts_edo_noeds_year / overallcosts_do_year) * 100, 4

    Login.ifNaN $('#login-calc-table-data td:eq(1)'), papercosts_do_year
    Login.ifNaN $('#login-calc-table-data td:eq(2)'), papercosts_edo_noeds_year
    Login.ifNaN $('#login-calc-table-data td:eq(3)'), papercosts_economy_noeds_percent, true
    Login.ifNaN $('#login-calc-table-data td:eq(5)'), MHcosts_do_year
    Login.ifNaN $('#login-calc-table-data td:eq(6)'), MHcosts_edo_year
    Login.ifNaN $('#login-calc-table-data td:eq(7)'), MHcosts_economy_percent, true
    Login.ifNaN $('#login-calc-table-data td:eq(9)'), postcosts_do_year
    Login.ifNaN $('#login-calc-table-data td:eq(10)'), postcosts_edo_year
    Login.ifNaN $('#login-calc-table-data td:eq(11)'), postcosts_economy_percent, true
    Login.ifNaN $('#login-calc-table-data td:eq(13)'), overallcosts_do_year
    Login.ifNaN $('#login-calc-table-data td:eq(14)'), overallcosts_edo_noeds_year
    Login.ifNaN $('#login-calc-table-data td:eq(15)'), overallcosts_economy_noeds_percent, true
    $('#login-calc-data1').html('<span>' + lists_year_do.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ") + '</span> листов бумаги')
    if isNaN overallcosts_do_year
      $('#login-calc-data2').html('<span>&mdash;</span> материалы, оформление и доставка')
    else
      $('#login-calc-data2').html('<span>' + overallcosts_do_year.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ") + ' руб.</span> материалы, оформление и доставка')

    $.ajax
      url: '/admin/calc_stat'
      data:
        data:
          addr_count: client_num
          salary: salary
          person_count: 0
          doc_count: docs_year
          sheet_per_doc: lists_doc_do
          paper_cost: list_pack_price
          tool_cost: cartridge_price
          tool_volume: cartridge_value
          delivery_cost: docsend_price_do
